<!--
  InstaLate (instant translation app)
  Copyright (C) 2021 Concept1Tech

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see https://www.gnu.org/licenses/.

  -->
<resources>
    <!-- content descriptions -->
    <string name="translation_trigger_button">Translation trigger button</string>
    <string name="language_1">Language 1</string>
    <string name="language_2">Language 2</string>
    <string name="translation_direction">Translation direction</string>
    <string name="go_to_settings">Go to settings</string>
    <string name="close_activity">Close activity</string>

    <string name="app_name">InstaLate</string>
    <string name="app_label">InstaLate Settings</string>
    <string name="show_settings">Show settings</string>
    <string name="invalid_search_term">Not a valid search term</string>
    <string name="network_error">No network.\n\nPlease enable your internet connection and try again.</string>
    <string name="transl_serv_notichan_name">InstaLate service notification channel</string>
    <string name="no_results_error">No results.</string>
    <string name="enabled">Enabled</string>
    <string name="disabled">Disabled</string>
    <string name="pref_provider">Provider</string>
    <string name="connection_error">Error.\nNo connection to provider.</string>
    <string name="no_data_error">Error.\nCan\'t get data from provider.</string>
    <string name="cb_service_desc">Required by InstaLate App to translate the clipboard</string>
    <string name="translation">Translation</string>
    <string name="version">Version</string>
    <string name="prefcat_general">General</string>
    <string name="pref_on_boot">Enable on boot</string>
    <string name="overlay_permission">InstaLate requires permission to display a floating button over other apps.\n\nYou can grant or revoke this permission in Android Settings/Apps.</string>
    <string name="settings">Settings</string>
    <string name="cancel">Cancel</string>
    <string name="pref_font_size_translation">Font size</string>
    <string name="pref_trigger_position">Button position</string>
    <string name="pref_trigger_duration">Button duration</string>
    <string name="choose_language">Choose language</string>
    <string name="choose_direction">Direction</string>
    <string name="about">About</string>
    <string name="transl_clipboard">Tap to translate clipboard</string>
    <string name="timeout_error">Timeout connecting to provider.\n\nPlease check your internet connection, try again later or change provider.</string>
    <string name="custom">Custom</string>
    <string name="translation_by">Translation\nby:</string>
    <string name="url">URL</string>
    <string name="link">Link</string>
    <string name="on">On</string>
    <string name="off">Off</string>
    <string name="floating_button">Floating Button</string>
    <string name="translation_on_clipboard_changes">Translate clipboard</string>
    <string name="provider_doesnt_support_language">Provider doesn\'t support language. Switching…</string>
    <string name="provider_doesnt_support_direction">Provider doesn\'t support translation direction. Switching…</string>
    <string name="provider_doesnt_support_language_combination">Provider doesn\'t support language combination. Switching…</string>
    <string name="source_code">Source code</string>
    <string name="licenses">Licenses</string>
    <string name="flag_icons">Flag icons (modified)</string>
    <string name="report_bug">Report a bug</string>
    <string name="material_design_icons">Material design icons</string>
    <string name="jsoup_html_parser">Jsoup: Java HTML Parser</string>
    <string name="androidx">AndroidX</string>
    <string name="material_components">Material components</string>
    <string name="pref_opacity_translation">Background opacity</string>

    <string-array name="pro_names">
        <item>WikDict</item>
        <item>Dict.cc</item>
        <item>Linguee (external sources)</item>
        <item>Beolingus</item>
        <item>Wiktionary (interwiki links)</item>
        <item>GCIDE (monolingual)</item>
        <item>Heinzelnisse</item>
        <item>DeepL (browser only)</item>
        <item>LibreTranslate</item>
    </string-array>

    <string-array name="dicts_languages">
        <item>English</item>
        <item>French</item>
        <item>Czech</item>
        <item>Polish</item>
        <item>Slovak</item>
        <item>German</item>
        <item>Hungarian</item>
        <item>Dutch</item>
        <item>Albanian</item>
        <item>Russian</item>
        <item>Spanish</item>
        <item>Swedish</item>
        <item>Icelandic</item>
        <item>Norwegian</item>
        <item>Italian</item>
        <item>Finnish</item>
        <item>Danish</item>
        <item>Portuguese</item>
        <item>Croatian</item>
        <item>Bulgarian</item>
        <item>Romanian</item>
        <item>Latin</item>
        <item>Esperanto</item>
        <item>Bosnian</item>
        <item>Turkish</item>
        <item>Serbian</item>
        <item>Greek</item>
        <item>Chinese</item>
        <item>Japanese</item>
        <item>Slovene</item>
        <item>Lithuanian</item>
        <item>Latvian</item>
        <item>Estonian</item>
        <item>Maltese</item>
        <item>Malagasy</item>
        <item>Indonesian</item>
        <item>Arabic</item>
        <item>Hindi</item>
        <item>Irish</item>
        <item>Korean</item>
        <item>Ukrainian</item>
        <item>Vietnamese</item>
    </string-array>


    <string-array name="dicts_directions">
        <item>bidirectional</item>
        <item>forward</item>
        <item>backward</item>
    </string-array>


    <string-array name="trigger_position">
        <item>Top left</item>
        <item>Top center</item>
        <item>Top right</item>
        <item>Center left</item>
        <item>Center</item>
        <item>Center right</item>
        <item>Bottom left</item>
        <item>Bottom center</item>
        <item>Bottom right</item>
    </string-array>


    <string-array name="trigger_duration">
        <item>1 Second</item>
        <item>2 Seconds</item>
        <item>3 Seconds (default)</item>
        <item>4 Seconds</item>
        <item>5 Seconds</item>
        <item>7 Seconds</item>
        <item>10 Seconds</item>
        <item>30 Seconds</item>
        <item>Show always</item>
    </string-array>


    <string-array name="fontsize_translation">
        <item>50%</item>
        <item>75%</item>
        <item>100% (default)</item>
        <item>125%</item>
        <item>150%</item>
        <item>200%</item>
    </string-array>


    <string-array name="fontsize_opacity">
        <item>0%</item>
        <item>10%</item>
        <item>20%</item>
        <item>30%</item>
        <item>40%</item>
        <item>50%</item>
        <item>60% (default)</item>
        <item>70%</item>
        <item>80%</item>
        <item>90%</item>
        <item>100%</item>
    </string-array>
</resources>
